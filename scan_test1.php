<?php
/**
 Author : Thitipong Samranvanich.   
 Note :   scan function in PHP  ok from "function" to  ";"  
 Since:  2x/11/2559   (year 2016)   
 Region : Thai
*/

$file1 = "test1.txt";
$project1  = "mtr59";

//$file2 = "file2.php";
$filenames = [$file1  /*,$file2 */ ] ;
$path1 = ".\\" ;
echo "\n=================================================================";
echo "\n  Scan File  by " .  htmlspecialchars($_SERVER["PHP_SELF"]); 
echo "\n  Scan PHP File (simple scan 'function' ) ";
echo "\n  " . date("Y-m-d H:i:s") ;
echo "\n=================================================================";

$lineno = 1 ;

foreach($filenames as $fidx =>$fname ){
	$path 	= $path1 . $fname ;
	$fp 	= new SplFileObjecT($path );
	echo "\n   Scan File "  .  ($fidx+1) . ". '$path' "; 
	if(! $fp->eof()){
		echo " OK  ";
	}

	$preview = true; 
	$found = true; 
	$rownum = 0 ;  
	while($found ){
		$a_ret = searchString("function");
		$found = $a_ret->found ; 
		//echo "\nFOUND = $found ";
		if($found ){
			$rownum += 1 ;
			//print_r($a_ret);
			$fp->fseek($a_ret->pos);
			$tk = getString(["{"],false);
			echo "\n  " . substr("0$rownum", -2) . " " . $tk ;
		}
	}
	echo "\n";
} // each file

//========================================================= 

//
echo "\r\n SCAN BY parseFunction (Still Error ! )\r\n";

$fp->rewind();
echo "\r\n Rewind to start of file";
//echo "\r\n\r\n";

$n = 0 ; 
for($i = 0 ; $i <=12 ; $i++){
	$p = searchString("<div class='td'>");
	$fp->fseek($p->pos);

	$s_tis620 = read_between("<div class='td'>","</div>");
	$posname = $s_tis620 ; 
	$s_tis620 = read_between("<div class='td'>","</div>");
	$fname = $s_tis620 ; 
	echo "\r\nposname=[$posname]";
	echo "\r\nfname=$fname";

	// echo $s_tis620 ;
	// $s_tis620 = read_between("class='td'>","</div>");
	// echo $s_tis620 ;
	$s_tis620 = read_between("/bossview/lst_atday/?uid=","'>");
	$strlen = strlen($s_tis620);
	$s_tis620 = substr($s_tis620,0,$strlen);
	echo "\r\nuid=" .  "," . $s_tis620 ;
	$n += 1; 
	// $temp = read_between("</div></div>","<div class='tr'>");
}

function read_between($str1, $str2){
global $fp ; 
	$pt = searchString($str1);
	//print_r($pt);
	$fp->fseek($pt->pos_end );
	//
	$pt2 = searchString($str2);
	$str2len =strlen($str2);
	$s_unknown = $fp->fread($pt2->pos_end - $pt->pos_end - $str2len);
	$encode = mb_detect_encoding($s_unknown , "auto");
	//echo "\n Encode = [$encode] \n";
	$s_echo  = iconv("utf-8","tis-620" , $s_unknown ); //from utf8 to tis620
	//echo "\ns:[$s_echo]";
	return $s_echo ; 
}



exit();


function parseFunction(){
	global $fp ,$fnrow   ; 
	$out  = [] ; 
	//$a_ret = searchString("function");
	$a_funct = ["function","public","private"] ;
	$a_ret = searchString_OneOfThen($a_funct);
	//print_r($a_ret);
	if($a_ret->found ){
		///print_r($a_ret);
		//print_r($a_ret->retobj);

		$fp->fseek($a_ret->retobj->pos);
		//print_r($a_ret);
		
		if( $a_ret->retobj->tk == "public" ){  acceptStr("public");  skipCharUntil("f"); $out[] = "PUBLIC" ;}
		if( $a_ret->retobj->tk == "private"  ){  acceptStr("private");  skipCharUntil("f");  $out[] = "PRIVATE"; }
		acceptStr("function");
		$out[] = "FUNCTION";
		skipSP();
		//echo $fp->fread(50);
		//exit();
		skipCharWhile([" "]);
		$fnName =getVar();
		$out[] = $fnName ; 
		$fnrow2 = substr("0" . $fnrow,-2);
		//echo "\nfnName $fnrow2 = $fnName";
		skipCharWhile([" "]);	
		acceptStr("(");
		$a_ret = searchString(")");
		$fp->fseek($a_ret->pos);
		//print_r($a_ret);
		acceptStr(")");
		$out[] =  "(..)";
		return (object)['found'=>true  , "a_out" => $out  , "err" =>""];
	}else{
		$debug1 = $fp->fread(50);
		return (object)['found'=>false , "a_out" => $out 
				, "err" => "expect [" . join(",",$a_funct) . "] , but found \n[[" . $debug1  . "]]"];
	}
}


// if found in $a_char return false
function getCh($a_char ){
	global $fp ; 
	//$tk = ""; 
	//$start  = $fp->ftell();
	$c = 	$fp->fgetc() ;	
	//$tk .= $c ; 
	if( ! in_array($c , $a_char )){
		return [false,$c]; 
	}else{
		return [true,$c]; 
	}
 }
 function lookAheadChar(){
	global $fp;
	$pos_save = $fp->ftell();
	$ch = $fp->fgetc();
	$fp->fseek($pos_save);
	return $ch ; 
 }
 function acceptStr($str){
	 global $fp; 
	 if(!lookAheadIsStr($str)){
		 $len = strlen($str);
		 $str2 = $fp->fread($len+20);
		 echo "\n Error Expect '$str' but found '$str2' ";
		 exit();
	 }else{
		 readStr($str);
	 }
 }
 function acceptStrOptional($str){
	 global $fp; 
	 if(lookAheadIsStr($str)){
		 readStr($str);
	 }
 }

 function lookAheadIsStr($str){
	global $fp;
	$len = strlen($str);
	$pos_save = $fp->ftell();
	$tk = $fp->fread($len) ; 
	if($tk == $str){
		$isStr = true; 
	}else{
		$isStr = false; 
	}
	$fp->fseek($pos_save);
	return $isStr ; 
 }
 function readStr($str){
	global $fp;
	$len = strlen($str);
	$tk =  $fp->fread($len) ; 
	if($tk == $str){
		$isStr = true; 
	}else{
		$isStr = false; 
	}
	return (object)['is_same'=> $isStr ,  'tk'=> $tk ] ; 	 
 }
 
 function skipCharUntil($arr_terminal){
	global $fp;	 	 
	$chnext = lookAheadChar();
	while( ! in_array($chnext , $arr_terminal)){
		$fp->fgetc();
	}
 }
 function skipCharWhile($arr_skip){
	global $fp;	 	 
	$chnext = lookAheadChar();
	while( in_array($chnext , $arr_skip)){
		$fp->fgetc();
		$chnext = lookAheadChar();
	}
}
function skipSP(){
	skipCharWhile([' ']);
}

 function getString($a_terminal, $preview = true ){
 	global $fp; 
	$pos = $fp->ftell();
	$tk = "";
 	list($isbreak,$c) = getCh($a_terminal);
	//echo "\n isbreak,c=[$isbreak,$c]";
	while(!$isbreak){
		//echo "\nAdd [$c]";
		$tk .= $c ; 
		list($isbreak,$c) = getCh($a_terminal);

	}
	if($preview){
		$fp->fseek($pos);
	}else{
		$fp->fseek(-1, SEEK_CUR );
	}
	if($isbreak){
		return $tk;  
	}
	return $tk; 
 }
 function getVar(){
	 $a_terminal = [' ','(','{' , '\n','\r'];
	 $tk = getString($a_terminal,false); 
	 return $tk ; 
 }
 
 /***
 	@return : object [found, pos,str,retobj] ,  NOTE retobj = [object found ,pos ,pos_end ,tk ] 
 ***/
 function searchString_OneOfThen($a_str){
	 global $fp;
	 assert(is_array($a_str));
	 $a_retobj = [] ; 
	 foreach($a_str as $i=> $str){
		 $a_retobj[$i] = searchString($str);
	 }
	 $minpos = 5000;
	 $min_str = "";
	 $min_retobj = (object)[];
	 $found = false;  
	 foreach($a_retobj as $i => $retobj){

		//echo "\nFound=$retobj->found str=$retobj->str";		
	 	if($retobj->found ){
			 //echo  "\n $retobj->found / $retobj->tk / $retobj->pos \n " ; 
			 //echo "F";
			 		//print_r($retobj );
			 //echo "\n $retobj->pos < $minpos\n" ; 		 
			 if(  $retobj->pos < $minpos ){
				// echo "SET ";
			 	$found =true ;
		 		$minpos =  $i ; 
			  	$min_str = $retobj->tk;
				$min_retobj = $retobj ;
			 }//   
		 }//
	 }
	 return (object)['found'=>$found ,  'pos' => $minpos , 'str'=>$min_str , 'retobj' => $min_retobj];
 }

 /* @return object found ,pos ,pos_end ,tk  */
 function searchString( $str   ){  
	 global $fp;
	 assert(is_string($str));
	 $pos_save = $fp->ftell();
	 $tk = "";  $len = strlen($str);
	 $bufsize = $len ;  
	 $found = false ;
	 $pos = -1 ;  
	 $loopnum = 0 ; 
	 while(!$fp->eof()){
		 $loopnum += 1 ; 
		 $c = $fp->fgetc();		 
		 $tk .= $c ;
		 $tk = substr($tk,-$len);
		 $tk_len = strlen($tk);
		// echo  "\ntk = $loopnum :  [$tk_len]"; 
		 if( $tk == $str){
			 $found =true ;
			 $pos = $fp->ftell() - $len ;
			 break;  //while 
		 }
	 }
	 $fp->fseek($pos_save);
	// echo "EOF";
	 $ret =  (object)['found'=>$found,'pos'=> $pos, 'pos_end'=>$pos+$len ,'tk'=> $tk];
	 //print_r($ret);
	 return $ret; 
 }



